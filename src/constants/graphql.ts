import gql from "graphql-tag";
// 2
export const ALL_USERS_QUERY = gql`
  query users {
    users {
      _id
      email
      username
    }
  }
`;

export const CREATE_USER_MUTATION = gql`
  # 2
  mutation signup($email: String!, $username: String!, $password: String!) {
    signup(email: $email, username: $username, password: $password)
  }
`;

export const LOGIN_USER_MUTATION = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password)
  }
`;

export const TOUR_TYPES = gql`
  query tourTypes {
    tourTypes {
      _id
      name
      code
    }
  }
`;

export const TOURIST_DESTINATIONS = gql`
  query touristDestinations($page: Int) {
    touristDestinations(page: $page) {
      _id
      tourType {
        _id
        name
        code
      }
      name
      file {
        _id
        originalName
        filename
        mimetype
        encoding
        path
      }
      location
      city
      tagCount
    }
  }
`;

export const TOURIRST_DESTINATION_SCORES = gql`
  query touristDestinationCores($id: String!) {
    touristDestinationCores(id: $id)
  }
`;

export const CREATE_TOURIST_DESTINATION = gql`
  mutation insertTouristDestination(
    $touristDestination: InputTouristDestination!
  ) {
    insertTouristDestination(touristDestination: $touristDestination)
  }
`;

export const CREATE_TOUR_TYPE = gql`
  mutation insertTourType($name: String!) {
    insertTourType(name: $name)
  }
`;

export const DELETE_TOUR_TYPE = gql`
  mutation deleteTourType($_id: String!) {
    deleteTourType(_id: $_id)
  }
`;

export const FILE_SINGLE_UPLOAD = gql`
  mutation singleUpload($file: Upload!) {
    singleUpload(file: $file) {
      _id
      originalName
      filename
      mimetype
      encoding
    }
  }
`;

export const VOTES = gql`
  query votes {
    votes {
      _id
      touristDestination {
        _id
        tourType {
          _id
          name
          code
        }
        name
        file {
          _id
          originalName
          filename
          mimetype
          encoding
          path
        }
        location
        city
      }
      score
      tagCount
    }
  }
`;

export const ADD_VOTE = gql`
  mutation addVote($touristDestination_id: String!, $score: Float!) {
    addVote(touristDestination_id: $touristDestination_id, score: $score)
  }
`;

export const VOTE = gql`
  query vote($touristDestination_id: String!) {
    vote(touristDestination_id: $touristDestination_id) {
      _id
      score
    }
  }
`;

export const AVERAGE_VOTE = gql`
  query averageVote($id: String!) {
    averageVote(id: $id)
  }
`;

export const PREDICTION_VOTE = gql`
  query predictionVote($touristDestination_id: String!) {
    predictionVote(touristDestination_id: $touristDestination_id) {
      _id
      score
    }
  }
`;

export const PREDICTION_VOTES = gql`
  query predictionVotes($page: Int) {
    predictionVotes(page: $page) {
      _id
      tourType {
        _id
        name
        code
      }
      name
      file {
        _id
        originalName
        filename
        mimetype
        encoding
        path
      }
      location
      city
      tagCount
    }
  }
`;

export const MY = gql`
  query my {
    my {
      votes {
        _id
        touristDestination {
          _id
          tourType {
            _id
            name
            code
          }
          name
          file {
            _id
            originalName
            filename
            mimetype
            encoding
            path
          }
          location
          city
          tagCount
        }
      }
      averageScore
      scores
      higherScore
      words
    }
  }
`;
