import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/src/stylus/app.styl";
//import '@mdi/font/css/materialdesignicons.css'
import "@fortawesome/fontawesome-free/css/all.css";

Vue.use(Vuetify, {
  iconfont: "fa"
});
