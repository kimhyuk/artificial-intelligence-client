import Vue from "vue";
import Router from "vue-router";
import Login from "@/views/Login.vue";
import Main from "@/views/Main.vue";
import store from "@store/index";
import { constants } from "@store/modules/user";

import admin from "./admin";
import main from "./main";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "login",
      component: Login
    },
    {
      path: "/logout",
      name: "logout",
      beforeEnter: function (to, from, next) {
        if (confirm("로그아웃 하시겠습니까?")) {
          store.dispatch(constants.NAME + "/" + constants.ACTIONS.DELETE_TOKEN);
          next({ path: "/" });
        } else {
          next(false);
        }
      }
    },
    main,
    admin
  ]
});
