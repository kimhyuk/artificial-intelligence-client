import TourVotes from "@components/TourVotes.vue";
import Recommands from "@components/Recommands.vue";
import My from "@components/My.vue";
import Main from "@/views/Main.vue";

export default {
  path: "/main",
  name: "main",
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: Main,
  children: [
    // UserHome will be rendered inside User's <router-view>
    // when /user/:id is matched
    { path: "", redirect: "tourvotes" },
    { path: "tourvotes", name: "tourvotes", component: TourVotes },
    { path: "recommands", name: "recommands", component: Recommands },
    { path: "my", name: "my", component: My }
  ]
};
