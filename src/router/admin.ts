import Tourist from "@/components/dashboard/container/Tourist.vue";
import Dashboard from "@/views/Dashboard.vue";
import TourType from "@/components/dashboard/container/TourType.vue";

export default {
  path: "/admin",
  name: "admin",
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: Dashboard,
  children: [
    { path: "tourist", name: "tourist", component: Tourist },
    { path: "tourtype", name: "tourtype", component: TourType }
  ]
};
