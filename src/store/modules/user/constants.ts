const COMMON = {
  SET_USER: "SET_USER"
};

export const ACTIONS = {
  SET_TOKEN: "SET_TOKEN",
  DELETE_TOKEN: "DELETE_TOKEN",
  GET_TOKEN: "GET_TOKEN",
  ...COMMON
};

export const MUTATIONS = {
  ...COMMON
};

export const GETTERS = {};

export const NAME = "user";

export default {
  ACTIONS,
  MUTATIONS,
  NAME
};
