import { MUTATIONS } from "./constants";
import { MutationTree } from "vuex";
import { IUserState } from "./types";
import electronStorage from "electron-json-storage";

// mutations
const mutations: MutationTree<IUserState> = {
  [MUTATIONS.SET_USER](state, payload) {
    state._id = payload._id;
    state.email = payload.email;
    state.username = payload.username;
  }
};

export default mutations;
