import { IUserState } from "./types";

const state: IUserState = {
  refreshToken: null,
  email: "",
  username: "",
  _id: ""
};

export default state;
