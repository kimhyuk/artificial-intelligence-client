import { GetterTree } from "vuex";
import { IUserState } from "./types";
import { IRootState } from "@/store/types";

// getters
const getters: GetterTree<IUserState, IRootState> = {};

export default getters;
