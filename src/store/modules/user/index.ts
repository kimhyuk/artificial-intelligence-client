import { IRootState } from "@/store/types";
import { Module } from "vuex";
import state from "./state";
import actions from "./actions";
import mutations from "./mutations";
import { IUserState } from "./types";
import constants from "./constants";

const user: Module<IUserState, IRootState> = {
  namespaced: true,
  state,
  actions,
  mutations
};

export { IUserState, constants };

export default user;
