export interface IUserState {
  refreshToken: string | null;
  _id: string | null;
  email: string | null;
  username: string | null;
}

export interface ITourType {
  name: string;
  code: string;
  _id: string;
}

export interface IInputFile {
  _id: string;
  path: string;
  filename: string;
  mimetype: string;
  encoding: string;
  originalName: string;
}

export interface ITouristDestination {
  _id: string;
  tourType: TourType;
  name: string;
  file: File;
  location: string;
  city: string;
  tagCount: string;
}

export class File implements IInputFile {
  _id = "";
  path = "";
  filename = "";
  mimetype = "";
  encoding = "";
  originalName = "";

  constructor(file?: IInputFile) {
    if (file) {
      this._id = file._id;
      this.path = file.path;
      this.filename = file.filename;
      this.mimetype = file.mimetype;
      this.encoding = file.encoding;
      this.originalName = file.originalName;
    }
  }
}

export class TourType implements ITourType {
  name = "";
  code = "";
  _id = "";

  constructor(tourType?: ITourType) {
    if (tourType) {
      this.name = tourType.name;
      this._id = tourType._id;
      this.code = tourType.code;
    }
  }
}

export class InputTouristDestination {
  _id = "";
  tourType = "";
  name = "";
  file = "";
  location = "";
  city = "";
  tagCount = "0";

  constructor(tourDestination?: ITouristDestination | InputTouristDestination) {
    if (tourDestination) {
      this._id = tourDestination._id;
      this.name = tourDestination.name;
      this.location = tourDestination.location;
      this.city = tourDestination.city;
      this.tagCount = tourDestination.tagCount;

      if (tourDestination instanceof InputTouristDestination) {
        this.tourType = tourDestination.tourType;
        this.file = tourDestination.file;
      } else {
        this.tourType = tourDestination.tourType._id;
        this.file = tourDestination.file._id;
      }
    }
  }
}

export class TouristDestination implements ITouristDestination {
  _id = "";
  tourType = new TourType();
  name = "";
  file = new File();
  location = "";
  city = "";
  tagCount = "0";

  constructor(tourDestination?: ITouristDestination) {
    if (tourDestination) {
      this._id = tourDestination._id;
      this.tourType = tourDestination.tourType;
      this.name = tourDestination.name;
      this.file = tourDestination.file;
      this.location = tourDestination.location;
      this.city = tourDestination.city;
      this.tagCount = tourDestination.tagCount;
    }
  }
}
