import { IEthereuemState } from "./types";

const state: IEthereuemState = {
  web3: undefined,
  coinbase: "",
  networkId: undefined,
  isListening: false,
  contract: undefined,
  balance: "0",
  isContract: false
};

export default state;
