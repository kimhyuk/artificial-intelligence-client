const COMMON = {
  SET_WEB3: "SET_WEB3",
  SET_STATUS: "SET_STATUS",
  SET_CONTRACT: "SET_CONTRACT",
  SET_IS_CONTRACT: "SET_IS_CONTRACT",
  REFRESH_BALANCE: "REFRESH_BALANCE",
  INIT: "INIT"
};

export const ACTIONS = {
  ...COMMON,
  SEND_COIN: "SEND_COIN"
};

export const MUTATIONS = {
  ...COMMON
};

export const GETTERS = {
  GET_SYMBOL_BALANCE: "GET_SYMBOL_BALANCE"
};

export const NAME = "ethereum";

export default {
  ACTIONS,
  MUTATIONS,
  NAME,
  GETTERS
};
