import { MUTATIONS } from "./constants";
import { MutationTree } from "vuex";
import { IEthereuemState } from "./types";

// mutations
const mutations: MutationTree<IEthereuemState> = {
  [MUTATIONS.INIT](state, payload: IEthereuemState) {
    state.web3 = payload.web3;
    state.coinbase = payload.coinbase;
    state.networkId = payload.networkId;
    state.isListening = payload.isListening;
  },

  [MUTATIONS.SET_WEB3](state, payload) {
    state.web3 = payload;
  },

  [MUTATIONS.REFRESH_BALANCE](state, balance) {
    if (state.web3)
      state.balance = state.web3.utils.fromWei(String(balance), "ether");
  },

  [MUTATIONS.SET_CONTRACT](state, payload) {
    state.contract = payload;
  },

  [MUTATIONS.SET_IS_CONTRACT](state, payload) {
    state.isContract = payload;
  }
};

export default mutations;
