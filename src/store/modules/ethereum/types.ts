import Web3 from "web3/types";
import { Contract } from "web3-eth-contract";

export interface IEthereuemState {
  web3?: Web3;
  coinbase?: string;
  networkId?: number;
  isListening?: boolean;
  contract?: Contract;
  balance?: string;
  isContract?: boolean;
}
