import { GetterTree } from "vuex";
import { IEthereuemState } from "./types";
import { IRootState } from "@/store/types";
import { GETTERS } from "./constants";

// getters
const getters: GetterTree<IEthereuemState, IRootState> = {
  [GETTERS.GET_SYMBOL_BALANCE]: state => {
    if (!state.isContract) {
      return String(state.balance) + "eth";
    } else {
      return String(state.balance) + "meta";
    }
  }
};

export default getters;
