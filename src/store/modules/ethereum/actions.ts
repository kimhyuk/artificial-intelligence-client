import { ACTIONS, MUTATIONS } from "./constants";
import { ActionTree } from "vuex";
import { IRootState } from "@/store/types";
import { IEthereuemState } from "./types";
import { TransactionReceipt } from "web3-core";

const actions: ActionTree<IEthereuemState, IRootState> = {
  [ACTIONS.INIT]({ commit }, payload) {
    commit(MUTATIONS.INIT, payload);
  },
  [ACTIONS.SET_WEB3]({ commit }, payload) {
    commit(MUTATIONS.SET_WEB3, payload);
  },
  [ACTIONS.SET_STATUS]({ commit }, payload) {
    commit(MUTATIONS.SET_STATUS, payload);
  },
  [ACTIONS.SET_CONTRACT]({ commit }, payload) {
    commit(MUTATIONS.SET_CONTRACT, payload);
  },
  [ACTIONS.SET_IS_CONTRACT]({ commit }, payload) {
    commit(MUTATIONS.SET_IS_CONTRACT, payload);
  },
  async [ACTIONS.REFRESH_BALANCE]({ state, commit }) {
    try {
      const { web3, contract, coinbase } = state;

      let balance: string | number = 0;
      if (!state.isContract) {
        if (!web3) throw new Error("web3 is undefined");
        if (!coinbase) throw new Error("No Address");

        balance = await web3.eth.getBalance(coinbase);
      } else {
        if (!contract) throw new Error("contract is undefined");

        balance = await contract.methods.balanceOf(state.coinbase).call();
      }
      commit(MUTATIONS.REFRESH_BALANCE, balance);
      return Promise.resolve();
    } catch (err) {
      return Promise.reject(err);
    }
  },

  async [ACTIONS.SEND_COIN]({ dispatch, state }, payload) {
    try {
      const { web3, coinbase, contract } = state;
      let hash: any;
      if (state.isContract) {
        if (!contract) throw new Error("contract is undefined");

        const { transfer } = contract.methods;
        const value = Number(payload.value) * Math.pow(10, 18);
        transfer(payload.to, String(value)).send(
          {
            from: coinbase
          },
          (error: any, transactionHash: any) => {
            alert(transactionHash);
          }
        );
      } else {
        if (!web3) throw new Error("web3 is undefined");
        web3.eth.sendTransaction({
          from: coinbase,
          to: payload.to,
          value: web3.utils.toWei(String(payload.value), "ether"),
          gasPrice: payload.gasPrice || web3.eth.defaultGasPrice,
          gas: payload.gas || web3.eth.defaultGas
        });
      }

      await dispatch(ACTIONS.REFRESH_BALANCE);
      return new Promise(resolve => resolve(hash));
    } catch (err) {
      return Promise.reject(err);
    }
  }
};

export default actions;
