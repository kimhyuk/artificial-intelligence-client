export interface IDashboardState {
  drawerVisible: boolean;
  snackbar: {
    visible: boolean;
    message: string | undefined;
  };
  dialogs: [];
}
