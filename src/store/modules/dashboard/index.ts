import { Module } from "vuex";
import { IDashboardState } from "./types";
import { IRootState } from "@/store/types";
import actions from "./actions";
import mutations from "./mutations";
import state from "./state";
import constants from "./constants";

const dashboard: Module<IDashboardState, IRootState> = {
  namespaced: true,
  state,
  actions,
  mutations
};

export { constants, IDashboardState };
export default dashboard;
