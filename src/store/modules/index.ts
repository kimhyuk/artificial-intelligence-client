import user from "./user";
import ethereum from "./ethereum";
import dashboard from "./dashboard";

export default {
  user,
  ethereum,
  dashboard
};
