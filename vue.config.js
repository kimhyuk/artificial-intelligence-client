const path = require("path");
module.exports = {
  chainWebpack: config => {
    config.resolve.alias.set("@", path.resolve(__dirname, "./src"));
    config.resolve.alias.set(
      "@components",
      path.resolve(__dirname, "./src/components")
    );
    config.resolve.alias.set(
      "@constants",
      path.resolve(__dirname, "./src/constants")
    );
    config.resolve.alias.set("@lang", path.resolve(__dirname, "./src/lang"));
    config.resolve.alias.set(
      "@layouts",
      path.resolve(__dirname, "./src/layouts")
    );
    config.resolve.alias.set(
      "@mixins",
      path.resolve(__dirname, "./src/mixins")
    );
    config.resolve.alias.set(
      "@plugins",
      path.resolve(__dirname, "./src/plugins")
    );

    config.resolve.alias.set(
      "@router",
      path.resolve(__dirname, "./src/router")
    );

    config.resolve.alias.set("@store", path.resolve(__dirname, "./src/store"));

    config.resolve.alias.set("@util", path.resolve(__dirname, "./src/util"));
    config.resolve.alias.set("@views", path.resolve(__dirname, "./src/views"));
  }
};
